package ${packageName}.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ${packageName}.dao.LoginDao;
import ${packageName}.entity.User;
import ${packageName}.service.LoginService;

@Service
public class LoginServiceImpl implements LoginService {

	private final LoginDao dao;

	@Autowired
	public LoginServiceImpl(LoginDao dao) {
		this.dao = dao;
	}

	@Override
	public User login(User user) {
		return dao.login(user);
	}

}
