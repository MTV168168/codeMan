package ${packageName}.config.mutidatasource;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * 数据源选择器
 *
 * @author zrx
 */
public class DynamicDataSourceHolder {
	private static final ThreadLocal<DataSourceType> DATA_SOURCE_HOLDER = new ThreadLocal<>();

	private static final Set<DataSourceType> DATA_SOURCE_TYPES = new HashSet<>();

	static {
		//添加全部枚举
		DATA_SOURCE_TYPES.addAll(Arrays.asList(DataSourceType.values()));
	}

	public static void setType(DataSourceType dataSourceType) {
		if (dataSourceType == null) {
			throw new NullPointerException();
		}
		DATA_SOURCE_HOLDER.set(dataSourceType);
	}

	public static DataSourceType getType() {
		return DATA_SOURCE_HOLDER.get();
	}

	static void clearType() {
		DATA_SOURCE_HOLDER.remove();
	}

	static boolean containsType(DataSourceType dataSourceType) {
		return DATA_SOURCE_TYPES.contains(dataSourceType);
	}
}
