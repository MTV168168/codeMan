<!DOCTYPE html>
<#if theme == "经典后台Thymleaf版">
<html lang="en" xmlns="http://www.w3.org/1999/xhtml" xmlns:th="http://www.thymeleaf.org">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>角色信息管理</title>
    <link rel="stylesheet" th:href="@{/mystatic/bootstrap/css/bootstrap.min-custom.css}">
    <link rel="stylesheet" th:href="@{/mystatic/bootstrap/css/bootstrap-table.css}">
    <link rel="stylesheet" th:href="@{/mystatic/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css}">
    <link rel="stylesheet" th:href="@{/mystatic/multiselect/css/bootstrap-multiselect.css}">
    <link rel="stylesheet" th:href="@{/mystatic/css/admin.css}">
    <link rel="stylesheet" th:href="@{/mystatic/css/ui.css}">
    <link rel="stylesheet" th:href="@{/mystatic/css/ui2.css}">
    <link rel="stylesheet" th:href="@{/mystatic/progressbar/css/mprogress.css}">
    <link rel="stylesheet" th:href="@{/mystatic/progressbar/css/style.css}">
    <link rel="stylesheet" th:href="@{/mystatic/ztree/css/bootstrapztree.css}" type="text/css">
    <link rel="stylesheet" th:href="@{/mystatic/ztree/css/personality.css}" type="text/css">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script th:src="@{/mystatic/html5shiv/html5shiv.min.js}"></script>
    <script th:src="@{/mystatic/respond/respond.min.js}"></script>
    <![endif]-->
    <style>
        /* 外面盒子样式---自己定义 */
        .page_div {
            margin: 20px 10px 20px 0;
            color: #666
        }

        /* 页数按钮样式 */
        .page_div button {
            display: inline-block;
            min-width: 30px;
            height: 28px;
            cursor: pointer;
            color: #666;
            font-size: 13px;
            line-height: 28px;
            background-color: #f9f9f9;
            border: 1px solid #dce0e0;
            text-align: center;
            margin: 0 4px;
            -webkit-appearance: none;
            -moz-appearance: none;
            appearance: none;
        }

        #firstPage, #lastPage, #nextPage, #prePage {
            width: 50px;
            color: #0073A9;
            border: 1px solid #0073A9
        }

        #nextPage, #prePage {
            width: 70px
        }

        .page_div .current {
            background-color: #0073A9;
            border-color: #0073A9;
            color: #FFF
        }

        /* 页面数量 */
        .totalPages {
            margin: 0 10px
        }

        .totalPages span, .totalSize span {
            color: #0073A9;
            margin: 0 5px
        }

        /*button禁用*/
        .page_div button:disabled {
            opacity: .5;
            cursor: no-drop
        }
    </style>
</head>
<#else>
<html lang="en">
<HEAD>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>角色信息管理</title>
    <meta name="keywords" content="">
    <meta name="description" content="">
    <link rel="shortcut icon" href="../favicon.ico">
    <link href="../../css/bootstrap.min-custom.css" rel="stylesheet">
    <link href="../../css/font-awesome.css?v=4.4.0" rel="stylesheet">
    <link href="../../css/plugins/bootstrap-table/bootstrap-table.min.css" rel="stylesheet">
    <link href="../../css/animate.css" rel="stylesheet">
    <link href="../../css/style.css?v=4.1.0" rel="stylesheet">
    <link href="../../css/plugins/pageMe/pageMe.css" rel="stylesheet"/>
    <link href="../../css/plugins/progressbar/mprogress.css" rel="stylesheet"/>
    <link href="../../css/plugins/progressbar/style.css" rel="stylesheet"/>
    <link rel="stylesheet" href="../../js/ztree/css/bootstrapztree.css" type="text/css">
    <link rel="stylesheet" href="../../js/ztree/css/personality.css" type="text/css">
</HEAD>
</#if>
<#if theme == "经典后台Thymleaf版">
<body id="body" class="admin-body toggle-nav fs" style="display: none">
    <div class="container-fluid">
        <div class="row" style="padding-left: 0">
            <div class="main" style="padding-top: 0">
                <div class="container-fluid">
                    <div class="admin-new-box ui-admin-content">
                        <!--后台正文区域-->
                        <div class="ui-panel">
                            <div class="ui-title-bar">
                                <div class="ui-title">角色管理模块</div>
                            </div>
                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane active" id="uiTab01">
                                    <!-- 查询区域 -->
                                    <div tagId="queryTag">
                                        <label for="roleName-query"></label><input type="text" id="roleName-query" placeholder="角色名称"
                                                                                   style="margin-top: 10px"/>&nbsp;
                                        <button type="button" class="btn btn-primary btn-sm" onclick="queryInfo()">查询</button>&nbsp;
                                    </div>
                                    <!-- 查询区域 end-->
                                    <!-- 添加数据区域 -->
                                    <div style="text-align: right;margin: 8px">
                                        <button tagId="addTag" type="button" class="btn btn-success btn-sm" onclick="addMsg()">添加数据</button>
                                    </div>
                                    <!-- 查询结果表格显示区域 -->
                                    <div class="table-responsive" style="overflow: scroll;">
                                        <table id="newsContent" class="table table-hover table-bordered text-nowrap">
                                            <tr>
                                                <th>操作</th>
                                                <th>角色id</th>
                                                <th>角色名称</th>
                                                <th>创建时间</th>
                                            </tr>
                                            <tbody id="dataTable">

                                            </tbody>
                                        </table>
                                        <div id="pageID" class="page_div"></div>
                                    </div>
                                    <!-- 查询结果表格显示区域 end-->
                                </div>
                            </div>
                        </div>
                        <!--后台正文区域结束-->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <#else>
    <body class="gray-bg">
    <div id="body" style="display: none">
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>角色信息管理</h5>
                </div>
                <div class="ibox-content">
                    <div class="row row-lg">
                        <div class="clearfix hidden-xs"></div>
                        <div class="col-sm-12">
                            <div class="example-wrap">
                                <!-- Tab panes -->
                                <!-- 查询区域 -->
                                <div tagId="queryTag">
                                    <label for="roleName-query"></label><input type="text" id="roleName-query" placeholder="角色名称"
                                                                               style="margin-top: 10px"/>&nbsp;
                                    <button type="button" class="btn btn-primary btn-sm" onclick="queryInfo()">查询</button>&nbsp;
                                </div>
                                <!-- 查询区域 end-->
                                <!-- 添加数据区域 -->
                                <div style="text-align: right;margin: 8px">
                                    <button tagId="addTag" type="button" class="btn btn-success btn-sm" onclick="addMsg()">添加数据</button>
                                </div>
                                <!-- 查询结果表格显示区域 -->
                                <div class="table-responsive" style="overflow: scroll;">
                                    <table id="newsContent" class="table table-hover table-bordered text-nowrap">
                                        <tr>
                                            <th>操作</th>
                                            <th>角色id</th>
                                            <th>角色名称</th>
                                            <th>创建时间</th>
                                        </tr>
                                        <tbody id="dataTable">

                                        </tbody>
                                    </table>
                                    <div id="pageID" class="page_div"></div>
                                </div>
                                <!-- 查询结果表格显示区域 end-->
                            </div>
                            <!--后台正文区域结束-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </#if>


        <!-- 修改模态框 -->
        <div class="modal fade" id="updateModal" tabindex="-1" role="dialog" aria-labelledby="updateModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h4 class="modal-title" id="updateModalLabel">修改信息</h4>
                    </div>
                    <div class="modal-body" id="updateModalBody">

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" onclick="confirmUp()">确认</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                    </div>
                </div>
            </div>
        </div>

        <!-- 添加模态框 -->
        <div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="addModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h4 class="modal-title" id="addModalLabel">添加信息</h4>
                    </div>
                    <div class="modal-body" id="addModalBody">
                        <form>
                            <div class="form-group">
                                <label for="roleName-insert" class="control-label">角色名称:</label>
                                <input type="text" class="form-control" id="roleName-insert"/>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" onclick="confirmAdd()">确认</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                    </div>
                </div>
            </div>
        </div>


        <!-- 修改内容模板  -->
        <script id="updateTemplate" type="text/html">
            <form>
                <div class="form-group">
                    <label for="roleName-attr" class="control-label">角色名称:</label>
                    <input type="text" class="form-control" id="roleName-attr" value="{{roleName}}"/>
                </div>
                <div class="form-group">
                    <label for="ztree" class="control-label">菜单权限（修改权限后，刷新浏览器页面方可生效！）:</label>
                    <ul id="ztree" class="ztree"></ul>
                </div>
                <div class="form-group">
                    <input type="hidden" class="form-control" id="roleId-attr" value="{{roleId}}"/>
                </div>
            </form>
        </script>

        <!-- 表格内容模板  -->
        <script id="tableContentTemplate" type="text/html">
            {{#result}}
            <tr>
                <td>
                    <span tagid="upTag"><button type="button" class="btn btn-info btn-sm" onclick="upMsg('{{roleId}}','/cmSysRole/')">更新</button>&nbsp;</span>
                    <span tagid="delTag"><button type="button" class="btn btn-danger btn-sm"
                                                 onclick="delMsg('{{roleId}}','/cmSysRole/delete',this)">删除</button>&nbsp;</span>
                </td>
                <td>{{roleId}}</td>
                <td>{{roleName}}</td>
                <td>{{createTime}}</td>
            </tr>
            {{/result}}
        </script>

        <#if theme == "经典后台Thymleaf版">
            <script th:src="@{/mystatic/ztree/js/jquery.min.js}"></script>
            <script th:src="@{/mystatic/ztree/js/jquery.ztree.core.js}"></script>
            <script th:src="@{/mystatic/ztree/js/jquery.ztree.excheck.js}"></script>
            <script th:src="@{/mystatic/ztree/js/jquery.ztree.exedit.js}"></script>
            <script th:src="@{/mystatic/js/pageMe.js}"></script>
            <script th:src="@{/mystatic/bootstrap/js/bootstrap.min.js}"></script>
            <script th:src="@{/mystatic/bootstrap/js/bootstrap-table.js}"></script>
            <script th:src="@{/mystatic/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js}"></script>
            <script th:src="@{/mystatic/bootstrap-datetimepicker/js/locales/bootstrap-datetimepicker.zh-CN.js}"></script>
            <script th:src="@{/mystatic/multiselect/js/bootstrap-multiselect.js}"></script>
            <script th:src="@{/mystatic/js/admin.js}"></script>
            <script th:src="@{/mystatic/js/ajaxFactory.js}"></script>
            <script th:src="@{/mystatic/js/crudFactory.js}"></script>
            <script th:src="@{/mystatic/js/jqAlert.js}"></script>
            <script th:src="@{/mystatic/echarts/echarts.min.js}"></script>
            <!-- 进度条  -->
            <script th:src="@{/mystatic/progressbar/js/mprogress.js}"></script>
            <script th:src="@{/mystatic/progressbar/js/init-mprogress.js}"></script>
            <script th:src="@{/mystatic/js/mustache/mustache.min.js}"></script>
            <script th:src="@{/mystatic/js/config.js}"></script>
        <#else>
            <script src="../../js/ztree/js/jquery.min.js"></script>
            <script src="../../js/ztree/js/jquery.ztree.core.js"></script>
            <script src="../../js/ztree/js/jquery.ztree.excheck.js"></script>
            <script src="../../js/ztree/js/jquery.ztree.exedit.js"></script>
            <script src="../../js/util/pageMe.js"></script>
            <script src="../../js/bootstrap.min.js"></script>
            <script src="../../js/plugins/bootstrap-table/bootstrap-table.min.js"></script>
            <script src="../../js/plugins/bootstrap-table/bootstrap-table-mobile.min.js"></script>
            <script src="../../js/plugins/bootstrap-table/locale/bootstrap-table-zh-CN.min.js"></script>
            <script src="../../js/util/ajaxFactory.js"></script>
            <script src="../../js/util/crudFactory.js"></script>
            <script src="../../js/plugins/layer/layer.min.js"></script>
            <script src="../../js/plugins/progressbar/init-mprogress.js"></script>
            <script src="../../js/plugins/progressbar/mprogress.js"></script>
            <script src="../../js/plugins/mustache/mustache.js"></script>
            <script src="../../js/config/config.js"></script>
        </#if>

        <script>
            var currentPage = 1;
            var totalPage;
            var sqlMap = {};
            //排序的数据
            var orderData = [];
            var controllerPrefix = "cmSysRole";
            var methodName = "likeSelect";

            $(function () {

                //后面可以根据自身业务具体添加查询条件，目前条件只有当前页
                //crudFactory.js
                $crud.getDataByCurrentPage();

            });

            var setting = {
                view: {
                    selectedMulti: true,
                    //不显示连线
                    showLine: false
                },
                check: {
                    enable: true
                },
                data: {
                    simpleData: {
                        enable: true,
                        idKey: "menuId",
                        pIdKey: "parentId",
                        rootPId: 0
                    }
                },
                edit: {
                    enable: true,
                    showRemoveBtn: false,
                    showRenameBtn: false,
                    drag: {
                        prev: false,
                        next: false,
                        inner: false
                    }
                }
            };

            //初始化菜单树
            function initTree(roleId) {
                //动态获取树形菜单
                $z.ajaxGet({
                    url: basePath + "/cmSysMenu/list-all",
                    data: {
                        roleId: roleId,
                        isAll: "YES"
                    },
                    beforeSend: function () {
                        InitMprogress();
                    },
                    success: function (data) {
                        $z.dealCommonResult(data, function () {
                            //赋值
                            zNodes = data.data;
                            //默认展开根节点
                            for (var i in zNodes) {
                                if (zNodes[i].isButton === 0) {
                                    zNodes[i].open = true;
                                }
                                if (roleId == zNodes[i].roleId) {
                                    zNodes[i].checked = true;
                                }
                            }
                            //初始化
                            $.fn.zTree.init($("#ztree"), setting, zNodes);
                            MprogressEnd();
                        });
                    }
                });
            }


            function queryInfo() {
                sqlMap = {};
                sqlMap.roleId = $("#roleId-query").val();
                sqlMap.roleName = $("#roleName-query").val();
                sqlMap.createTime = $("#createTime-query").val();
                sqlMap.updateTime = $("#updateTime-query").val();
                sqlMap.createUserId = $("#createUserId-query").val();
                sqlMap.updateUserId = $("#updateUserId-query").val();
                currentPage = 1;
                orderData = [];
                $crud.getDataByCurrentPage();
            }

            function delMsg(roleId, path, thisVal) {
                <#if theme == "经典后台Thymleaf版">
                $.MsgBox.Confirm("温馨提示", "执行删除后将无法恢复，确定继续吗？"
                <#else>
                layer.confirm("<em style='color:black'>" + '执行删除后将无法恢复，确定继续吗？' + "</em>", {
                        icon: 3,
                        offset: "200px",
                        title: '温馨提示'
                    }
                    </#if>
                    , function (<#if theme == "前后端分离响应式">index</#if>) {
                        $z.ajaxPost({
                            url: basePath + path,
                            data: {
                                roleId: roleId
                            },
                            success: function (data) {
                                $z.dealCommonResult(data, function () {
                                    $crud.getDataByCurrentPage();
                                    <#if theme == "经典后台Thymleaf版">
                                     $.MsgBox.Alert("提示", "删除成功！");
                                    <#else>
                                    layer.alert("<em style='color:black'>删除成功！</em>", {
                                        icon: 6,
                                        offset: "200px",
                                        title: '提示'
                                    });
                                    </#if>
                                });
                            }
                        });
                        <#if theme == "前后端分离响应式">
                        layer.close(index);
                        </#if>
                    });

            }

            function upMsg(roleId, path) {
                $z.ajaxGet({
                    url: basePath + path + roleId,
                    success: function (data) {
                        $z.dealCommonResult(data, function () {
                            data = data.data;
                            // 把数据动态写入模态框
                            var bodyHtmlTemplate = $("#updateTemplate").html();
                            Mustache.parse(bodyHtmlTemplate); // 预编译模板
                            var bodyHtml = Mustache.render(bodyHtmlTemplate, data);
                            $('#updateModalBody').html(bodyHtml);
                            //初始化菜单树
                            initTree(roleId);
                            $('#updateModal').modal('show');
                        });

                    }
                });
            }

            function confirmUp() {
                //获取被选中的菜单
                var treeObj = $.fn.zTree.getZTreeObj("ztree");
                var nodes = treeObj.getCheckedNodes(true);
                var roleId = $("#roleId-attr").val();
                //如果是超级管理员
                var canDelNum = 0;
                if (roleId == 1) {
                    for (var i in nodes) {
                        if (nodes[i].canDel == 0) {
                            canDelNum += 1;
                        }
                    }
                    if (canDelNum !== 4) {
                        <#if theme == "经典后台Thymleaf版">
                        $.MsgBox.Alert("提示", "超级管理员角色必须具备系统，用户，菜单和角色管理权限！");
                        <#else>
                        layer.alert("<em style='color:black'>超级管理员角色必须具备系统，用户，菜单和角色管理权限！</em>", {
                            icon: 5,
                            offset: "200px",
                            title: '错误'
                        });
                        </#if>
                        return;
                    }
                }
                $z.ajaxPost({
                    url: basePath + "/" + controllerPrefix + "/update",
                    data: {
                        roleId: roleId,
                        roleName: $("#roleName-attr").val(),
                        cmSysMenuEntityList: nodes
                    },
                    success: function (data) {
                        $z.dealCommonResult(data, function () {
                            <#if theme == "经典后台Thymleaf版">
                            $.MsgBox.Alert("提示", "更新成功，刷新浏览器页面权限方可生效！");
                            <#else>
                            layer.alert("<em style='color:black'>更新成功，刷新浏览器页面权限方可生效！</em>", {
                                icon: 6,
                                offset: "200px",
                                title: '提示'
                            });
                            </#if>
                            $('#updateModal').modal('hide');
                            $crud.getDataByCurrentPage();
                        });
                    }
                });
            }

            function addMsg() {
                $('#addModal').modal('show');
            }

            function confirmAdd() {
                $z.ajaxPost({
                    url: basePath + "/" + controllerPrefix + "/add",
                    data: {
                        roleId: $("#roleId-insert").val(),
                        roleName: $("#roleName-insert").val(),
                        createTime: $("#createTime-insert").val(),
                        updateTime: $("#updateTime-insert").val(),
                        createUserId: $("#createUserId-insert").val(),
                        updateUserId: $("#updateUserId-insert").val()
                    },
                    success: function (data) {
                        $z.dealCommonResult(data, function () {
                            <#if theme == "经典后台Thymleaf版">
                            $.MsgBox.Alert("提示", "添加成功！");
                            <#else>
                            layer.alert("<em style='color:black'>添加成功！</em>", {
                                icon: 6,
                                offset: "200px",
                                title: '提示'
                            });
                            </#if>
                            $('#addModal').modal('hide');
                            //初始化
                            sqlMap = {};
                            currentPage = 1;
                            $crud.getDataByCurrentPage();
                        });
                    }
                });
            }

            function makeResult(data) {
                for (var i = 0; i < data.length; i++) {
                    if (data[i] == null) {
                        data[i] = {};
                        data[i]["roleId"] = "无";
                        data[i]["roleName"] = "无";
                        data[i]["createTime"] = "无";
                        data[i]["updateTime"] = "无";
                        data[i]["createUserId"] = "无";
                        data[i]["updateUserId"] = "无";
                    }
                }
            }
        </script>
    <#if theme == "前后端分离响应式">
        </div>
    </#if>
    </body>
</html>



