<!DOCTYPE html>
<#if theme == "经典后台Thymleaf版">
<html lang="en" xmlns="http://www.w3.org/1999/xhtml" xmlns:th="http://www.thymeleaf.org">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>用户信息管理</title>
    <link rel="stylesheet" th:href="@{/mystatic/bootstrap/css/bootstrap.min-custom.css}">
    <link rel="stylesheet" th:href="@{/mystatic/bootstrap/css/bootstrap-table.css}">
    <link rel="stylesheet" th:href="@{/mystatic/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css}">
    <link rel="stylesheet" th:href="@{/mystatic/multiselect/css/bootstrap-multiselect.css}">
    <link rel="stylesheet" th:href="@{/mystatic/css/admin.css}">
    <link rel="stylesheet" th:href="@{/mystatic/css/ui.css}">
    <link rel="stylesheet" th:href="@{/mystatic/css/ui2.css}">
    <link rel="stylesheet" th:href="@{/mystatic/progressbar/css/mprogress.css}">
    <link rel="stylesheet" th:href="@{/mystatic/progressbar/css/style.css}">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script th:src="@{/mystatic/html5shiv/html5shiv.min.js}"></script>
    <script th:src="@{/mystatic/respond/respond.min.js}"></script>
    <![endif]-->
    <style>
        /* 外面盒子样式---自己定义 */
        .page_div {
            margin: 20px 10px 20px 0;
            color: #666
        }

        /* 页数按钮样式 */
        .page_div button {
            display: inline-block;
            min-width: 30px;
            height: 28px;
            cursor: pointer;
            color: #666;
            font-size: 13px;
            line-height: 28px;
            background-color: #f9f9f9;
            border: 1px solid #dce0e0;
            text-align: center;
            margin: 0 4px;
            -webkit-appearance: none;
            -moz-appearance: none;
            appearance: none;
        }

        #firstPage, #lastPage, #nextPage, #prePage {
            width: 50px;
            color: #0073A9;
            border: 1px solid #0073A9
        }

        #nextPage, #prePage {
            width: 70px
        }

        .page_div .current {
            background-color: #0073A9;
            border-color: #0073A9;
            color: #FFF
        }

        /* 页面数量 */
        .totalPages {
            margin: 0 10px
        }

        .totalPages span, .totalSize span {
            color: #0073A9;
            margin: 0 5px
        }

        /*button禁用*/
        .page_div button:disabled {
            opacity: .5;
            cursor: no-drop
        }
    </style>
</head>
<#else>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>用户信息管理</title>
    <meta name="keywords" content="">
    <meta name="description" content="">
    <link rel="shortcut icon" href="../favicon.ico">
    <link href="../../css/bootstrap.min-custom.css" rel="stylesheet">
    <link href="../../css/font-awesome.css?v=4.4.0" rel="stylesheet">
    <link href="../../css/plugins/bootstrap-table/bootstrap-table.min.css" rel="stylesheet">
    <link href="../../css/animate.css" rel="stylesheet">
    <link href="../../css/style.css?v=4.1.0" rel="stylesheet">
    <link href="../../css/plugins/pageMe/pageMe.css" rel="stylesheet"/>
    <link href="../../css/plugins/progressbar/mprogress.css" rel="stylesheet"/>
    <link href="../../css/plugins/progressbar/style.css" rel="stylesheet"/>
</head>
</#if>

<#if theme == "经典后台Thymleaf版">
<body id="body" class="admin-body toggle-nav fs" style="display: none">
    <div class="container-fluid">
        <div class="row" style="padding-left: 0">
            <div class="main" style="padding-top: 0">
                <div class="container-fluid">
                    <div class="admin-new-box ui-admin-content">
                        <!--后台正文区域-->
                        <div class="ui-panel">
                            <div class="ui-title-bar">
                                <div class="ui-title">用户管理模块</div>
                            </div>
                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane active" id="uiTab01">
                                    <!-- 查询区域 -->
                                    <div tagId="queryTag">
                                        <label for="username-query"></label><input type="text" id="username-query" placeholder="姓名"
                                                                                   style="margin-top: 10px"/>&nbsp;
                                        <button type="button" class="btn btn-primary btn-sm" onclick="queryInfo()">查询</button>&nbsp;
                                    </div>
                                    <!-- 查询区域 end-->
                                    <!-- 添加数据区域 -->
                                    <div style="text-align: right; margin: 8px">
                                        <button tagId="addTag" type="button" class="btn btn-success btn-sm" onclick="addMsg()">添加数据</button>
                                    </div>
                                    <!-- 查询结果表格显示区域 -->
                                    <div class="table-responsive" style="overflow: scroll;">
                                        <table id="newsContent" class="table table-hover table-bordered text-nowrap">
                                            <tr>
                                                <th>操作</th>
                                                <th>用户名</th>
                                                <th>角色</th>
                                                <th>创建时间</th>
                                            </tr>
                                            <tbody id="dataTable">

                                            </tbody>
                                        </table>
                                        <div id="pageID" class="page_div"></div>
                                    </div>
                                    <!-- 查询结果表格显示区域 end-->
                                </div>
                            </div>
                        </div>
                        <!--后台正文区域结束-->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <#else>
    <body class="gray-bg">
    <div id="body" style="display: none">
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>用户信息管理</h5>
                </div>
                <div class="ibox-content">
                    <div class="row row-lg">
                        <div class="clearfix hidden-xs"></div>
                        <div class="col-sm-12">
                            <div class="example-wrap">
                                <!-- 查询区域 -->
                                <div tagId="queryTag">
                                    <label for="username-query"></label><input type="text" id="username-query" placeholder="姓名"
                                                                               style="margin-top: 10px"/>&nbsp;
                                    <button type="button" class="btn btn-primary btn-sm" onclick="queryInfo()">查询</button>&nbsp;
                                </div>
                                <!-- 查询区域 end-->
                                <!-- 添加数据区域 -->
                                <div style="text-align: right; margin: 8px">
                                    <button tagId="addTag" type="button" class="btn btn-success btn-sm" onclick="addMsg()">添加数据</button>
                                </div>
                                <!-- 查询结果表格显示区域 -->
                                <div class="table-responsive" style="overflow: scroll;">
                                    <table id="newsContent" class="table table-hover table-bordered text-nowrap">
                                        <tr>
                                            <th>操作</th>
                                            <th>用户名</th>
                                            <th>角色</th>
                                            <th>创建时间</th>
                                        </tr>
                                        <tbody id="dataTable">

                                        </tbody>
                                    </table>
                                    <div id="pageID" class="page_div"></div>
                                </div>
                                <!-- 查询结果表格显示区域 end-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </#if>


        <!-- 修改模态框 -->
        <div class="modal fade" id="updateModal" tabindex="-1" role="dialog" aria-labelledby="updateModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h4 class="modal-title" id="updateModalLabel">修改信息</h4>
                    </div>
                    <div class="modal-body" id="updateModalBody">

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" onclick="confirmUp()">确认</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                    </div>
                </div>
            </div>
        </div>

        <!-- 添加模态框 -->
        <div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="addModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h4 class="modal-title" id="addModalLabel">添加信息</h4>
                    </div>
                    <div class="modal-body" id="addModalBody">
                        <form>
                            <div class="form-group">
                                <label for="username-insert" class="control-label">用户名&ensp;<label style="color: red">*</label></label>
                                <input type="text" class="form-control" id="username-insert"/>
                            </div>
                            <div class="form-group">
                                <label for="roleId-insert" class="control-label">角色&ensp;<label style="color: red">*</label></label>
                                <select id="roleId-insert" class="form-control">
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="password-insert" class="control-label">密码&ensp;<label style="color: red">*</label></label>
                                <input type="text" class="form-control" id="password-insert"/>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" onclick="confirmAdd()">确认</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                    </div>
                </div>
            </div>
        </div>

        <!--角色列表模板-->
        <script id="roleListTemplate" type="text/html">
            {{#.}}
            <option value="{{roleId}}">{{roleName}}</option>
            {{/.}}
        </script>

        <!-- 修改内容模板  -->
        <script id="updateTemplate" type="text/html">
            <form>
                <div class="form-group">
                    <label for="username-attr" class="control-label">用户名&ensp;<label style="color: red">*</label></label>
                    <input type="text" class="form-control" id="username-attr" value="{{username}}"/>
                </div>
                <div class="form-group">
                    <label for="roleId-attr" class="control-label">角色&ensp;<label style="color: red">*</label></label>
                    <select id="roleId-attr" class="form-control">
                        {{#roleList}}
                        <option value="{{roleId}}">{{roleName}}</option>
                        {{/roleList}}
                    </select>
                </div>
                <div class="form-group">
                    <input type="hidden" class="form-control" id="userId-attr" value="{{userId}}"/>
                </div>
            </form>
        </script>

        <!-- 表格内容模板  -->
        <script id="tableContentTemplate" type="text/html">
            {{#result}}
            <tr>
                <td>
                    <span tagId="upTag"><button type="button" class="btn btn-info btn-sm"
                                                onclick="upMsg('{{userId}}','/cmSysUser/select')">更新</button>&nbsp;</span>
                    <span tagId="delTag"><button type="button" class="btn btn-danger btn-sm"
                                                 onclick="delMsg('{{userId}}','/cmSysUser/delete',this)">删除</button>&nbsp;</span>
                </td>
                <td>{{username}}</td>
                <td>{{roleName}}</td>
                <td>{{createTime}}</td>
            </tr>
            {{/result}}
        </script>

        <#if theme == "经典后台Thymleaf版">
            <script th:src="@{/mystatic/jquery/jquery.min.js}"></script>
            <script th:src="@{/mystatic/js/pageMe.js}"></script>
            <script th:src="@{/mystatic/bootstrap/js/bootstrap.min.js}"></script>
            <script th:src="@{/mystatic/bootstrap/js/bootstrap-table.js}"></script>
            <script th:src="@{/mystatic/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js}"></script>
            <script th:src="@{/mystatic/bootstrap-datetimepicker/js/locales/bootstrap-datetimepicker.zh-CN.js}"></script>
            <script th:src="@{/mystatic/multiselect/js/bootstrap-multiselect.js}"></script>
            <script th:src="@{/mystatic/js/admin.js}"></script>
            <script th:src="@{/mystatic/js/ajaxFactory.js}"></script>
            <script th:src="@{/mystatic/js/crudFactory.js}"></script>
            <script th:src="@{/mystatic/js/jqAlert.js}"></script>
            <script th:src="@{/mystatic/echarts/echarts.min.js}"></script>
            <!-- 进度条  -->
            <script th:src="@{/mystatic/progressbar/js/mprogress.js}"></script>
            <script th:src="@{/mystatic/progressbar/js/init-mprogress.js}"></script>
            <script th:src="@{/mystatic/js/mustache/mustache.min.js}"></script>
            <script th:src="@{/mystatic/js/config.js}"></script>
            <script th:src="@{/mystatic/encrypt/md5.js}"></script>
        <#else>
            <script src="../../js/ztree/js/jquery.min.js"></script>
            <script src="../../js/util/pageMe.js"></script>
            <script src="../../js/bootstrap.min.js"></script>
            <script src="../../js/plugins/bootstrap-table/bootstrap-table.min.js"></script>
            <script src="../../js/plugins/bootstrap-table/bootstrap-table-mobile.min.js"></script>
            <script src="../../js/plugins/bootstrap-table/locale/bootstrap-table-zh-CN.min.js"></script>
            <script src="../../js/util/ajaxFactory.js"></script>
            <script src="../../js/util/crudFactory.js"></script>
            <script src="../../js/plugins/layer/layer.min.js"></script>
            <script src="../../js/plugins/progressbar/init-mprogress.js"></script>
            <script src="../../js/plugins/progressbar/mprogress.js"></script>
            <script src="../../js/plugins/mustache/mustache.js"></script>
            <script src="../../js/config/config.js"></script>
            <script src="../../js/encrypt/md5.js"></script>
        </#if>

        <script>
            var currentPage = 1;
            var totalPage;
            var sqlMap = {};
            var controllerPrefix = "cmSysUser";

            /**
             * 分页带roleName的方法（basePath和pageSize在config.js中配置）
             */
            function pageRole() {
                sqlMap.currentPage = currentPage;
                sqlMap.pageSize = pageSize;
                $z.ajaxGet({
                    url: basePath + "/" + controllerPrefix + "/page-role",
                    data: sqlMap,
                    async: false,
                    beforeSend: function () {
                        // 显示进度条
                        InitMprogress();
                    },
                    success: function (data) {
                        $z.dealCommonResult(data, function () {
                            data = data.data;
                            var $pageId = $("#pageID");
                            var $dataTable = $('#dataTable');
                            if (data.result.length === 0) {
                                <#if theme == "经典后台Thymleaf版">
                                $.MsgBox.Alert("提示", "没有您想要查询的数据！");
                                <#else>
                                layer.alert("<em style='color:black'>没有您想要查询的数据</em>", {
                                    icon: 5,
                                    offset: "200px",
                                    title: '提示'
                                });
                                </#if>
                                MprogressEnd();
                                $dataTable.empty();
                                $pageId.empty();
                                return;
                            }
                            currentPage = data.currentPage;
                            totalPage = data.totalPage;
                            $pageId.empty();
                            // pageMe.js 使用方法
                            $pageId.paging({
                                pageNum: currentPage, // 当前页面
                                totalNum: totalPage, // 总页码
                                totalList: data.count, // 记录总数量
                                callback: function (num) { // 回调函数
                                    currentPage = num;
                                    pageRole();
                                }
                            });
                            var bodyHtmlTemplate = $("#tableContentTemplate").html();
                            Mustache.parse(bodyHtmlTemplate); // 预编译模板
                            var bodyHtml = Mustache.render(bodyHtmlTemplate, data);
                            $dataTable.html(bodyHtml);
                            // 进度条消失
                            MprogressEnd();
                            //根据权限显示按钮
                            showButtonByRole();
                        });
                    }
                });
            }

            $(function () {
                pageRole();
            });


            var roleList = [];
            //获取所有的角色
            $z.ajaxGet({
                url: basePath + "/cmSysRole/list-all",
                success: function (data) {
                    //把返回的角色列表赋值给roleList
                    roleList = data.data;
                    //渲染roleList模板
                    var roleListTemplate = $("#roleListTemplate").html();
                    Mustache.parse(roleListTemplate); // 预编译模板
                    var bodyHtml = Mustache.render(roleListTemplate, roleList);
                    //添加数据模态框角色选择添加
                    $("#roleId-insert").html(bodyHtml);
                }
            });

            function queryInfo() {
                sqlMap = {};
                sqlMap.username = $("#username-query").val();
                currentPage = 1;
                pageRole();
            }


            function delMsg(userId, path, thisVal) {

                <#if theme == "经典后台Thymleaf版">
                $.MsgBox.Confirm("温馨提示", "执行删除后将无法恢复，确定继续吗？"
                <#else>
                layer.confirm("<em style='color:black'>" + '执行删除后将无法恢复，确定继续吗？' + "</em>", {
                        icon: 3,
                        offset: "200px",
                        title: '温馨提示'
                    }
                    </#if>
                    , function (<#if theme == "前后端分离响应式">index</#if>) {
                        $z.ajaxPost({
                            url: basePath + path,
                            data: {
                                userId: userId
                            },
                            success: function (data) {
                                $z.dealCommonResult(data, function () {
                                    pageRole();
                                    <#if theme == "经典后台Thymleaf版">
                                     $.MsgBox.Alert("提示", "删除成功！");
                                    <#else>
                                    layer.alert("<em style='color:black'>删除成功！</em>", {
                                        icon: 6,
                                        offset: "200px",
                                        title: '提示'
                                    });
                                    </#if>
                                });
                            }
                        });
                        <#if theme == "前后端分离响应式">
                        layer.close(index);
                        </#if>
                    });

            }

            function upMsg(userId, path) {
                $z.ajaxPost({
                    url: basePath + path,
                    data: {
                        userId: userId
                    },
                    success: function (data) {
                        $z.dealCommonResult(data, function () {
                            data = data.data;
                            data[0].roleList = roleList;
                            // 把数据动态写入模态框
                            var bodyHtmlTemplate = $("#updateTemplate").html();
                            Mustache.parse(bodyHtmlTemplate); // 预编译模板
                            var bodyHtml = Mustache.render(bodyHtmlTemplate, data[0]);
                            $('#updateModalBody').html(bodyHtml);
                            $("#roleId-attr").val(data[0].roleId);
                            $('#updateModal').modal('show');
                        });

                    }
                });
            }

            function confirmUp() {
                $z.ajaxPost({
                    url: basePath + "/" + controllerPrefix + "/update",
                    data: {
                        userId: $("#userId-attr").val(),
                        roleId: $("#roleId-attr").val(),
                        username: $("#username-attr").val()
                    },
                    success: function (data) {
                        $z.dealCommonResult(data, function () {
                            <#if theme == "经典后台Thymleaf版">
                            $.MsgBox.Alert("提示", "更新成功！");
                            <#else>
                            layer.alert("<em style='color:black'>更新成功！</em>", {
                                icon: 6,
                                offset: "200px",
                                title: '提示'
                            });
                            </#if>
                            $('#updateModal').modal('hide');
                            pageRole();
                        });
                    }
                });
            }


            function addMsg() {
                $('#addModal').modal('show');
            }

            function confirmAdd() {
                var $pwd = $("#password-insert").val();
                if ($pwd === undefined || $pwd === '') {
                    <#if theme == "经典后台Thymleaf版">
                    $.MsgBox.Alert("错误", '请输入密码！');
                    <#else>
                    layer.alert("<em style='color:black'>请输入密码！</em>", {
                        icon: 5,
                        offset: "200px",
                        title: '错误'
                    });
                    </#if>
                    return;
                }
                $z.ajaxPost({
                    url: basePath + "/" + controllerPrefix + "/add",
                    data: {
                        roleId: $("#roleId-insert").val(),
                        username: $("#username-insert").val(),
                        //采用md5加密
                        password: hex_md5($pwd)
                    },
                    success: function (data) {
                        $z.dealCommonResult(data, function () {
                            <#if theme == "经典后台Thymleaf版">
                            $.MsgBox.Alert("提示", "添加成功！");
                            <#else>
                            layer.alert("<em style='color:black'>添加成功！</em>", {
                                icon: 6,
                                offset: "200px",
                                title: '错误'
                            });
                            </#if>
                            $('#addModal').modal('hide');
                            //初始化
                            sqlMap = {};
                            currentPage = 1;
                            pageRole();
                        });
                    }
                });
            }
        </script>
    <#if theme == "前后端分离响应式">
        </div>
    </#if>
    </body>
</html>



