package ${packageName}.service;

import ${packageName}.entity.CmSysButtonEntity;

public interface CmSysButtonService {

	void add(CmSysButtonEntity entity);
	
	void delete(CmSysButtonEntity entity);
	
	void update(CmSysButtonEntity entity);
	
}
