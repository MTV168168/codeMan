package ${packageName}.dao;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import ${packageName}.entity.CmSysUserEntity;

import java.util.List;

@Mapper
@Repository
public interface CmSysUserDao extends BaseDao<CmSysUserEntity> {

	/**
	 * 分页获取用户（带roleName）
	 *
	 * @param cmSysUserEntity
	 * @return
	 */
	List<CmSysUserEntity> pageRole(CmSysUserEntity cmSysUserEntity);

	/**
	 * 分页获取用户数量（带roleName）
	 *
	 * @param cmSysUserEntity
	 * @return
	 */
	Long pageRoleCount(CmSysUserEntity cmSysUserEntity);

	/**
	 * 根据用户名获取
	 * @param username
	 * @return
	 */
	CmSysUserEntity getByUserName(@Param("username") String username);

	/**
	 * 根据id获取
	 * @param userId
	 * @return
	 */
	CmSysUserEntity getById(@Param("userId") Long userId);
}
