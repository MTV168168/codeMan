package ${packageName}.dao;

import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;
import ${packageName}.entity.CmSysButtonEntity;

@Mapper
@Repository
public interface CmSysButtonDao extends BaseDao<CmSysButtonEntity> {

}
