package ${packageName}.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ${packageName}.constant.MoveType;
import ${packageName}.entity.CmSysMenuEntity;

/**
 * @ClassName NodeMoveDto
 * @Author zrx
 * @Date 2021/6/29 16:41
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class NodeMoveDto {
	@ApiModelProperty("被移动的节点")
	private CmSysMenuEntity moveMenu;
	@ApiModelProperty("目标节点")
	private CmSysMenuEntity targetMenu;
	@ApiModelProperty("移动类型")
	private MoveType moveType;
}
