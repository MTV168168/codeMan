package ${packageName}.dao;

<#if frameWorkVal=="springBoot">
import org.apache.ibatis.annotations.Mapper;
</#if>
import org.springframework.stereotype.Repository;

import ${packageName}.entity.User;

<#if frameWorkVal=="springBoot">
@Mapper
</#if>
@Repository
public interface LoginDao {

	User login(User user);

}
