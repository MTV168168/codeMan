package ${packageName}.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
<#if ifUseSwagger == "是">
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
</#if>
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.EqualsAndHashCode;
import java.io.Serializable;
<#--如果className不为空-->
<#if hasList??>
import java.util.List;
</#if>

/**
 * ${makeEntityName_cn}
 */
<#if ifUseSwagger == "是">
@ApiModel
</#if>
@Data
@EqualsAndHashCode(callSuper = false)
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class ${makeEntityName?cap_first} extends CommonEntity implements Serializable {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = 1L;

<#list MakeEntityModels as makeEntityModel>
    /**
	 *  ${makeEntityModel.filedName_cn}
	 */
	<#if ifUseSwagger == "是">
	@ApiModelProperty(value = "${makeEntityModel.filedName_cn}", name = "${makeEntityModel.filedName_eng}")
	</#if>
    private ${makeEntityModel.serviceType} ${makeEntityModel.filedName_eng};
</#list>
}
