package ${projectName};

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
<#if isMutiDataSource>import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;</#if>
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication<#if isMutiDataSource>(exclude = {DataSourceAutoConfiguration.class})</#if>
@EnableAsync
@EnableScheduling
public class ${captureProjectName}Application {

	public static void main(String[] args) {
		SpringApplication.run(${captureProjectName}Application.class, args);
	}

}
